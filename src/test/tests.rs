#[cfg(test)]
mod tests {

    //Test cases with real fetched data
    //Testing sorting algorithm with hardcoded values
    //Received and Expected should always be same, if algorithm will be
    //changed or improved

    use crate::hub;
    use std::sync::{Mutex, MutexGuard};
    use std::collections::HashMap;
    use crate::hub::Sorted;
    use protos::orderbook::{Summary, Level};

    //First case test common behaviour of sort
    #[test]
    fn test_sorted_different() {

        //HashMap placed in Mutex to make shipping possible of
        //MutexGuard since such a dependency is used in the hub logic
        let aggregeted: Mutex<HashMap<String, Sorted>> = Mutex::new(HashMap::new());

        let example_data_binance = hub::Sorted { asks: vec![vec![7908.15, 0.007723], vec![7912.12, 0.00383]], bids: vec![vec![7880.38, 0.058311], vec![7876.89, 0.016524]] };
        let example_data_bitstamp = hub::Sorted { asks: vec![vec![7897.45, 2.0], vec![7897.83, 6.811]], bids: vec![vec![7707.03, 0.0035], vec![7707.77, 0.055]] };

        //Hardcoded Summary in correct order as it
        //Should be after sort
        let mut expected_summary = Summary::new();
        expected_summary.asks.push(
            Level {
                exchange: "bitstamp".parse().unwrap(),
                price: 7897.45,
                amount: 2.0,
                unknown_fields: Default::default(),
                cached_size: Default::default()
            });
        expected_summary.asks.push(
            Level {
                exchange: "binance".parse().unwrap(),
                price: 7908.15,
                amount: 0.007723,
                unknown_fields: Default::default(),
                cached_size: Default::default()
            });
        expected_summary.asks.push(
            Level {
                exchange: "bitstamp".parse().unwrap(),
                price: 7897.83,
                amount: 6.811,
                unknown_fields: Default::default(),
                cached_size: Default::default()
            });
        expected_summary.asks.push(
            Level {
                exchange: "binance".parse().unwrap(),
                price: 7912.12,
                amount: 0.00383,
                unknown_fields: Default::default(),
                cached_size: Default::default()
            });

        expected_summary.bids.push(
            Level {
                exchange: "binance".parse().unwrap(),
                price: 7880.38,
                amount: 0.058311,
                unknown_fields: Default::default(),
                cached_size: Default::default()
            });
        expected_summary.bids.push(
            Level {
                exchange: "bitstamp".parse().unwrap(),
                price: 7707.03,
                amount: 0.0035,
                unknown_fields: Default::default(),
                cached_size: Default::default()
            });

        expected_summary.bids.push(
            Level {
                exchange: "binance".parse().unwrap(),
                price: 7876.89,
                amount: 0.016524,
                unknown_fields: Default::default(),
                cached_size: Default::default()
            });

        expected_summary.bids.push(
            Level {
                exchange: "bitstamp".parse().unwrap(),
                price: 7707.77,
                amount: 0.055,
                unknown_fields: Default::default(),
                cached_size: Default::default()
            });

        expected_summary.spread = 17.06999999999971;

        aggregeted.lock().unwrap().insert("binance".to_owned(), example_data_binance);
        aggregeted.lock().unwrap().insert("bitstamp".to_owned(), example_data_bitstamp);
        let received_sum = hub::Hub::new().sorted_summary(aggregeted.lock().unwrap());
        assert_eq!(expected_summary, received_sum)
    }

    //Some of data, such as price can be equal
    //to test the choice in favor of amount
    #[test]
    fn test_sorted_with_equal_price() {
        let aggregeted: Mutex<HashMap<String, Sorted>> = Mutex::new(HashMap::new());

        let example_data_binance = hub::Sorted { asks: vec![vec![7960.09, 0.005556], vec![7964.67, 0.001472]], bids: vec![vec![7923.14, 0.108325], vec![7889.23, 0.017375]] };
        let example_data_bitstamp = hub::Sorted { asks: vec![vec![7960.09, 0.63885827], vec![7899.83, 0.5]], bids: vec![vec![7893.85, 2.064178], vec![7890.7, 2.34]] };

        let mut expected_summary = Summary::new();
        expected_summary.asks.push(
            Level {
                exchange: "bitstamp".parse().unwrap(),
                price: 7960.09,
                amount: 0.63885827,
                unknown_fields: Default::default(),
                cached_size: Default::default()
            });
        expected_summary.asks.push(
            Level {
                exchange: "binance".parse().unwrap(),
                price: 7960.09,
                amount: 0.005556,
                unknown_fields: Default::default(),
                cached_size: Default::default()
            });
        expected_summary.asks.push(
            Level {
                exchange: "bitstamp".parse().unwrap(),
                price: 7899.83,
                amount: 0.5,
                unknown_fields: Default::default(),
                cached_size: Default::default()
            });
        expected_summary.asks.push(
            Level {
                exchange: "binance".parse().unwrap(),
                price: 7964.67,
                amount: 0.001472,
                unknown_fields: Default::default(),
                cached_size: Default::default()
            });

        expected_summary.bids.push(
            Level {
                exchange: "binance".parse().unwrap(),
                price: 7923.14,
                amount: 0.108325,
                unknown_fields: Default::default(),
                cached_size: Default::default()
            });
        expected_summary.bids.push(
            Level {
                exchange: "bitstamp".parse().unwrap(),
                price: 7893.85,
                amount: 2.064178,
                unknown_fields: Default::default(),
                cached_size: Default::default()
            });

        expected_summary.bids.push(
            Level {
                exchange: "bitstamp".parse().unwrap(),
                price: 7890.7,
                amount: 2.34,
                unknown_fields: Default::default(),
                cached_size: Default::default()
            });

        expected_summary.bids.push(
            Level {
                exchange: "binance".parse().unwrap(),
                price: 7889.23,
                amount: 0.017375,
                unknown_fields: Default::default(),
                cached_size: Default::default()
            });

        expected_summary.spread = 36.94999999999982;

        aggregeted.lock().unwrap().insert("binance".to_owned(), example_data_binance);
        aggregeted.lock().unwrap().insert("bitstamp".to_owned(), example_data_bitstamp);
        let received_sum = hub::Hub::new().sorted_summary(aggregeted.lock().unwrap());
        assert_eq!(expected_summary, received_sum)
    }
}