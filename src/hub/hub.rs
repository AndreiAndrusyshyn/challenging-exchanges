use tokio::sync::mpsc;
use tokio::sync::mpsc::{Sender as ChannelSender, Receiver as ChannelReceiver};
use serde::{Deserialize, Serialize};
use tokio::task;
use tokio::sync::watch;
use tokio::sync::watch::{Sender as watchSender, Receiver as watchReceiver};
use std::io::stdin;
use tokio::select;
use serde_json::{Value, Map};
use std::borrow::Borrow;
use std::collections::HashMap;
use std::cmp::Ordering::{Less, Greater, Equal};
use protos::orderbook::{Level, Summary, Empty};
use tonic::{Status, Response, Request};
use tonic::transport::Server;
use protos::orderbook::orderbook_aggregator_server::{OrderbookAggregator, OrderbookAggregatorServer};

use crate::hub::service::Service;

//Pairs that are common for both exchanges
const CURRENCIE_PAIRS: [&str; 7] = ["btceur", "xrpeur", "xrpbtc", "ltcbtc", "etheur", "ethbtc", "bchbtc"];
const BINANCE: &'static str = "binance";
const BITSTAMP: &'static str = "bitstamp";

//lastUpdateId in such format is used only for proper parsing
#[derive(Serialize, Deserialize)]
struct Binance {
    lastUpdateId: i64,
    asks: Vec<Vec<String>>,
    bids: Vec<Vec<String>>,
}

//Bistamp provide data in for of Objects
//it only can be parsed in map with Value of serde_json
#[derive(Serialize, Deserialize)]
struct BitstampAgregation {
    data: Map<String, Value>
}

//After sorting from lowest to highest for asks and swapped for bids
//it should be saved for easy transport in the channel
//struct as wrapper was used
#[derive(Debug)]
pub struct Sorted {
    pub asks: Vec<Vec<f64>>,
    pub bids: Vec<Vec<f64>>,
}

#[derive(Clone)]
struct OrderbookService {
    rec: watchReceiver<Option<Summary>>,
}

#[tonic::async_trait]
impl OrderbookAggregator for OrderbookService {
    type BookSummaryStream = mpsc::Receiver<Result<Summary, Status>>;

    async fn book_summary(&self, _request: Request<Empty>) -> Result<Response<Self::BookSummaryStream>, Status> {
        let (mut tx, rx) = mpsc::channel(4);
        let mut this = self.clone();
        tokio::spawn(async move {
            let mut current_summary: Summary = Summary { asks: vec![], bids: vec![], spread: 0.0 };
            loop {
                match this.rec.recv().await.unwrap() {
                    Some(inside) => {
                        if current_summary != inside.clone() {
                            current_summary = inside.clone();
                            tx.send(Ok(inside)).await;
                        }
                    }
                    _ => ()
                };
            }
        });
        Ok(Response::new(rx))
    }
}

#[derive(Debug, Clone, PartialEq)]
enum Signal {
    Up,
    Down,
}

#[derive(Clone)]
pub struct Hub {}

impl Hub {
    pub fn new() -> Self { Hub {} }

    pub async fn run(self) {
        let (send_book_first_service, mut sorted_values_from_first_service) = mpsc::channel(1);
        let (send_book_second_service, mut sorted_values_from_second_service) = mpsc::channel(1);

        let first_service = Service::new();
        let second_service = Service::new();

        self.clone().start_fetch_info(self.clone().start_cli().await,
                                      send_book_first_service,
                                      send_book_second_service,
                                      first_service,
                                      second_service).await;

        let (send_summary_to_grpc, receive_summary_in_grpc) = watch::channel(None);

        self.clone().transfer_to_grpc(sorted_values_from_first_service,
                                      sorted_values_from_second_service,
                                      send_summary_to_grpc).await;
        self.start_grpc_server(receive_summary_in_grpc).await;
    }

    async fn start_fetch_info(self, initial_pair: Vec<String>, send_sorted_book1: ChannelSender<Sorted>,
                              send_sorted_book2: ChannelSender<Sorted>,
                              first_service: Service,
                              second_service: Service) {
        task::spawn(async move {
            let mut current_pair: Vec<String> = initial_pair;
            let mut new_pair: Vec<String> = vec![];
            let mut current_binance_url = String::from("wss://stream.binance.com:9443/ws/".to_owned() + current_pair.get(0).unwrap() + "@depth20@100ms");
            let current_bitstamp_url = String::from("wss://ws.bitstamp.net");

            let mut current_bitstamp_message = r#"{
    "event": "bts:subscribe",
    "data": {
        "channel": "order_book_"#.to_owned() + current_pair.get(0).unwrap() + r#""
    }
}"#;
            loop {
                let (send_book_first_service, receive_book_first_service) = mpsc::channel(1);
                let (send_book_second_service, receive_book_second_service) = mpsc::channel(1);
                let (mut send_semaphore_channel_first_service, receive_semaphore_channel_first_service) = watch::channel(Signal::Up);
                let (mut send_semaphore_channel_second_service, receive_semaphore_channel_second_service) = watch::channel(Signal::Up);

                select! {
                c = task::spawn({self.clone().start_cli()}) => {
                send_semaphore_channel_first_service.broadcast(Signal::Down);
                send_semaphore_channel_first_service.closed();
                send_semaphore_channel_second_service.broadcast(Signal::Down);
                send_semaphore_channel_second_service.closed();
                new_pair = c.unwrap();

                 }
                _ = task::spawn(first_service.clone().start(current_binance_url.clone(), None, send_book_first_service.clone(), current_pair.clone()))=> {println!("cli conka")}
                _ = task::spawn(second_service.clone().start(current_bitstamp_url.clone(), Some(current_bitstamp_message.clone()), send_book_second_service.clone(), current_pair.clone()))=> {println!("cli conka")}
                _ = task::spawn(self.clone().fetch_info(receive_book_first_service, receive_book_second_service, receive_semaphore_channel_first_service,receive_semaphore_channel_second_service , send_sorted_book1.clone(), send_sorted_book2.clone())) => {println!("done")}
                }

                current_binance_url = self.clone().new_url(current_binance_url.clone(), current_pair.clone(), new_pair.clone());
                current_bitstamp_message = self.clone().update_message(current_bitstamp_message.clone(), current_pair.clone(), new_pair.clone());
                current_pair = new_pair.clone();
            }
        });
    }

    async fn transfer_to_grpc(self, mut sorted_first_service: ChannelReceiver<Sorted>, mut sorted_first_second: ChannelReceiver<Sorted>, book_sender: watchSender<Option<Summary>>) {
        task::spawn(async move {
            loop {
                let mut holder: HashMap<String, Sorted> = HashMap::new();
                let (first, second) = tokio::join!(
                    sorted_first_service.recv(),
                    sorted_first_second.recv());

                holder.insert(BINANCE.to_owned(), first.unwrap());
                holder.insert(BITSTAMP.to_owned(), second.unwrap());
                book_sender.broadcast(Some(self.clone().sorted_summary(holder)));
            }
        });
    }

    async fn start_grpc_server(self, book_receiver: watchReceiver<Option<Summary>>) {
        task::spawn(async move {
            let addr = "127.0.0.1:10000".parse().unwrap();
            let route_guide = OrderbookService {
                rec: book_receiver
            };
            let svc = OrderbookAggregatorServer::new(route_guide);
            Server::builder().add_service(svc).serve(addr).await;
        }).await;
    }

    fn inject_in_level(self,
                       bids_asks: Option<&Sorted>,
                       exchange: &str) -> (Vec<Level>, Vec<Level>) {
        let mut binance_asks = vec![];
        let mut binance_bids = vec![];
        for sorted_element in bids_asks {
            for price_amount_pair in sorted_element.asks.clone().iter() {
                for (prices, amounts) in price_amount_pair.get(0).iter().zip(price_amount_pair.get(1).iter()) {
                    binance_asks.push(Level { price: **prices, amount: **amounts, exchange: exchange.to_owned() });
                }
            }
            for price_amount_pair in sorted_element.bids.clone().iter() {
                for (prices, amounts) in price_amount_pair.get(0).iter().zip(price_amount_pair.get(1).iter()) {
                    binance_bids.push(Level { price: **prices, amount: **amounts, exchange: exchange.to_owned() });
                }
            }
        }
        (binance_asks, binance_bids)
    }

    pub fn sorted_summary(self, bids_asks: HashMap<String, Sorted>) -> Summary {
        let mut binance_asks = vec![];
        let mut binance_bids = vec![];

        let mut bitstamp_asks = vec![];
        let mut bitstamp_bids = vec![];

        let mut sorted_asks = vec![];
        let mut sorted_bids = vec![];

        if bids_asks.contains_key(BINANCE) {
            let get_sorted_for_key = bids_asks.get(BINANCE);
            let (asks, bids)
                = self.clone()
                .inject_in_level(get_sorted_for_key, BINANCE);
            binance_asks = asks;
            binance_bids = bids;
        }

        if bids_asks.contains_key(BITSTAMP) {
            let get_sorted_for_key = bids_asks.get(BITSTAMP);
            let (asks, bids)
                = self.clone()
                .inject_in_level(get_sorted_for_key, BITSTAMP);
            bitstamp_asks = asks;
            bitstamp_bids = bids;
        }

        //Double check to prevent unexpected behaviors
        if !binance_asks.is_empty() && !bitstamp_asks.is_empty() {
            for (a, b) in binance_asks.iter().zip(bitstamp_asks.iter()) {
                match a.price.partial_cmp(b.price.borrow()) {
                    Some(Less) => {
                        sorted_asks.push(a.to_owned());
                        sorted_asks.push(b.to_owned());
                    }
                    Some(Greater) => {
                        sorted_asks.push(b.to_owned());
                        sorted_asks.push(a.to_owned());
                    }
                    Some(Equal) => {
                        match a.amount.partial_cmp(b.amount.borrow()) {
                            Some(Less) => {
                                sorted_asks.push(b.to_owned());
                                sorted_asks.push(a.to_owned());
                            }
                            Some(Greater) => {
                                sorted_asks.push(a.to_owned());
                                sorted_asks.push(b.to_owned());
                            }
                            _ => {
                                sorted_asks.push(a.to_owned());
                                sorted_asks.push(b.to_owned());
                            }
                        }
                    }
                    _ => {}
                };
            }
        }

        if !binance_bids.is_empty() && !bitstamp_bids.is_empty() {
            for (a, b) in binance_bids.iter().zip(bitstamp_bids.iter()) {
                match a.price.partial_cmp(b.price.borrow()) {
                    Some(Less) => {
                        sorted_bids.push(b.to_owned());
                        sorted_bids.push(a.to_owned());
                    }
                    Some(Greater) => {
                        sorted_bids.push(a.to_owned());
                        sorted_bids.push(b.to_owned());
                    }
                    Some(Equal) => {
                        match a.amount.partial_cmp(b.amount.borrow()) {
                            Some(Less) => {
                                sorted_bids.push(b.to_owned());
                                sorted_bids.push(a.to_owned());
                            }
                            Some(Greater) => {
                                sorted_bids.push(a.to_owned());
                                sorted_bids.push(b.to_owned());
                            }
                            _ => {
                                sorted_bids.push(a.to_owned());
                                sorted_bids.push(b.to_owned());
                            }
                        }
                    }
                    _ => {}
                };
            }
        }
        sorted_bids.truncate(10);
        sorted_asks.truncate(10);
        Summary { asks: sorted_asks.clone(), bids: sorted_bids.clone(), spread: sorted_asks.get(0).unwrap().price - sorted_bids.get(0).unwrap().price }
    }
    async fn fetch_info(self, mut receive_book_first_service: ChannelReceiver<String>,
                        mut receive_book_second_service: ChannelReceiver<String>,
                        semaphore_recv_first_service: watchReceiver<Signal>,
                        semaphore_recv_second_service: watchReceiver<Signal>,
                        mut send_sorted_book_first_service: ChannelSender<Sorted>,
                        mut send_sorted_book_second_service: ChannelSender<Sorted>,
    )
    {
        let this = self.clone();
        tokio::spawn(async move {
            while let Some(value) = receive_book_first_service.recv().await {
                // println!("received service 1 = {:?}", value.is_empty());
                send_sorted_book_first_service.send(self.clone().sort_first_service(value)).await;
                let s = semaphore_recv_first_service.borrow();
                if *s == Signal::Down {
                    receive_book_first_service.close();
                }
            }
        });
        tokio::spawn(async move {
            while let Some(value) = receive_book_second_service.recv().await {
                match this.clone().sort_second_service(value) {
                    Some(Sorted) => { send_sorted_book_second_service.send(Sorted).await; }
                    _ => ()
                }
                let s = semaphore_recv_second_service.borrow();
                if *s == Signal::Down {
                    receive_book_second_service.close();
                }
            }
        }).await;
    }
    fn new_url(self, CONNECTION_URL: String, current_pair: Vec<String>, new_pair: Vec<String>) -> String {
        CONNECTION_URL.replace(current_pair.join("").trim(), new_pair.join("").trim())
    }

    fn update_message(self, message: String, current_pair: Vec<String>, new_pair: Vec<String>) -> String {
        message.replace(current_pair.join("").trim(), new_pair.join("").trim())
    }

    fn sort_first_service(self, received_book: String) -> Sorted
    {
        let mut v: Binance = serde_json::from_str(received_book.as_ref()).unwrap();
        let mut asks_converted_from_string
            = v.asks.iter_mut()
            .map(|c| c.iter_mut()
                .map(|d| d.parse::<f64>().unwrap())
                .collect::<Vec<_>>())
            .collect::<Vec<Vec<_>>>();

        //sorting vector for ask from lowest to highest
        asks_converted_from_string.sort_by(|a, b| a.partial_cmp(b.borrow()).unwrap());

        let mut bids_converted_from_string
            = v.bids.iter_mut()
            .map(|c| c.iter_mut()
                .map(|d| d.parse::<f64>().unwrap())
                .collect::<Vec<_>>())
            .collect::<Vec<Vec<_>>>();

        //and opposite for bids
        bids_converted_from_string.sort_by(|a, b| b.partial_cmp(a.borrow()).unwrap());

        //after data is sorted it can be send to pushed it
        //HashMap aggregator that later will be send to stream
        Sorted { asks: asks_converted_from_string, bids: bids_converted_from_string }
    }
    fn sort_second_service(self, received_book: String) -> Option<Sorted> {
        let mut v: BitstampAgregation = serde_json::from_str(received_book.trim()).unwrap();
        if v.data.contains_key("asks") {
            let ask_serde_value = v.data["asks"].as_array_mut().unwrap();
            let mut ask_opening_first_vector
                = ask_serde_value.iter_mut()
                .map(|c| c.as_array_mut().unwrap()).collect::<Vec<_>>();

            let mut ask_opening_nested_vector
                = ask_opening_first_vector.iter_mut()
                .map(|c| c.iter_mut()
                    .map(|c| c.as_str().unwrap())
                    .collect::<Vec<_>>()).collect::<Vec<Vec<_>>>();

            let mut ask_converted_string_to_float
                = ask_opening_nested_vector.iter_mut()
                .map(|c| c.iter_mut()
                    .map(|d| d.parse::<f64>().unwrap())
                    .collect::<Vec<_>>()).collect::<Vec<Vec<_>>>();

            ask_converted_string_to_float.sort_by(|a, b| a.partial_cmp(b.borrow()).unwrap());

            let bid_serde_value = v.data["bids"].as_array_mut().unwrap();
            let mut bid_opening_first_vector
                = bid_serde_value.iter_mut()
                .map(|c| c.as_array_mut().unwrap()).collect::<Vec<_>>();

            let mut bid_opening_nested_vector
                = bid_opening_first_vector.iter_mut()
                .map(|c| c.iter_mut()
                    .map(|c| c.as_str().unwrap())
                    .collect::<Vec<_>>()).collect::<Vec<Vec<_>>>();

            let mut bid_converted_string_to_float
                = bid_opening_nested_vector.iter_mut()
                .map(|c| c.iter_mut()
                    .map(|d| d.parse::<f64>().unwrap()).collect::<Vec<_>>()).collect::<Vec<Vec<_>>>();
            bid_converted_string_to_float.sort_by(|a, b| b.partial_cmp(a.borrow()).unwrap());

            Some(Sorted { asks: ask_converted_string_to_float, bids: bid_converted_string_to_float })
        } else { None }
    }

    async fn start_cli(self) -> Vec<String> {
        let mut input_first_pair = String::new();
        let mut input_second_pair = String::new();
        loop {
            input_first_pair.clear();
            input_second_pair.clear();
            println!("Waiting for new pair!");

            stdin().read_line(&mut input_first_pair).unwrap();
            stdin().read_line(&mut input_second_pair).unwrap();
            let first_pair_trimmed = input_first_pair.trim();
            let second_pair_trimmed = input_second_pair.trim();
            let pair_join = first_pair_trimmed.to_owned() + second_pair_trimmed;
            if CURRENCIE_PAIRS.iter().any(|v| v == &pair_join.trim()) {
                return vec![pair_join];
            } else {
                println!("Pair doesn't exist, available pairs is {:?}", CURRENCIE_PAIRS);
            }
        }
    }
}
