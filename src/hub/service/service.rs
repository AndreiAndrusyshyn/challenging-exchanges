use tokio_tungstenite::connect_async;
use tokio_tungstenite::tungstenite::Message;
use tokio::sync::mpsc;
use tokio::sync::mpsc::{Sender as ChannelSender};
use futures::stream::StreamExt;
use futures::future::{self};
use futures_util::{pin_mut};

#[derive(Clone)]
pub struct Service {}

impl Service {
    pub fn new() -> Self { Service{} }

    pub async fn start(self, connection_addres: String, message: Option<String>, send_fetched_data:ChannelSender<String>, pair_to_fetch: Vec<String>){

        let url = url::Url::parse(&connection_addres).unwrap();
        let (mut write_to_server, read_message_for_server) = mpsc::channel(1);

        println!("[Opened connection to]: {:?} Pair to fetch: {:?}", url, pair_to_fetch);
        let (websocket_stream, _) = connect_async(url).await.expect("Failed to connect");
        println!("WebSocket handshake has been successfully completed");

        let (websocket_write,
            websocket_read) = websocket_stream.split();

        let stream_to_websocket_server = read_message_for_server.map(Ok).forward(websocket_write);
        if  connection_addres.contains("ws.bitstamp.net") {
            if let Some(x) = message {
                write_to_server.send(Message::Text(x.to_owned())).await;
            };
        };

        let websocket_read_message = {
            websocket_read.for_each(|message| async {
                match message.unwrap() {
                    //.println!("Message without format :{:?}", e);
                    Message::Text(e) => { send_fetched_data.clone().try_send(e); },
                    //println!("Connection is closed!");
                    Message::Close(e) => {println!("Connection is closed!: {:?}", e);},
                    _ => ()
                };
            })
        };
        pin_mut!(stream_to_websocket_server, websocket_read_message);
        future::select(stream_to_websocket_server, websocket_read_message).await;
    }

}
