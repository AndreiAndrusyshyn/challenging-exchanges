# challenging-exchanges
Service for streaming sorted order book from exchanges with following architecture.
![architecture](https://gitlab.com/AndreiAndrusyshyn/challenging-exchanges/-/raw/master/img/architecture.jpg)

## Prerequisites

```bash
sudo apt-get install -y curl
```
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
## Start

Install all packages & run

```bash
cargo build
```
